package questionOneAndTwo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Question1Test {

    Question1 question1 = new Question1();


    @org.junit.jupiter.api.Test
    void getingSumOfTwoStringNumbers() {
        assertEquals("10", question1.getingSumOfTwoStringNumbers("5","5"));
        assertEquals("1000000", question1.getingSumOfTwoStringNumbers("500000","500000"));
        assertEquals("10000000", question1.getingSumOfTwoStringNumbers("5000000","5000000"));
        assertEquals("1000000000", question1.getingSumOfTwoStringNumbers("500000000","500000000"));
        assertEquals("100000000000000000", question1.getingSumOfTwoStringNumbers("50000000000000000","50000000000000000"));
    }

    @org.junit.jupiter.api.Test
    void getingMultiplicationOfTwoStringNumbers() {
        assertEquals("25", question1.getingMultiplicationOfTwoStringNumbers("5", "5"));
        assertEquals("27951696", question1.getingMultiplicationOfTwoStringNumbers("52344", "534"));
        assertEquals("2829315696", question1.getingMultiplicationOfTwoStringNumbers("5298344", "534"));
        assertEquals("27954832716", question1.getingMultiplicationOfTwoStringNumbers("52349874", "534"));

        assertEquals("2813282968566336", question1.getingMultiplicationOfTwoStringNumbers("523498877664", "5374"));
    }




    @org.junit.jupiter.api.Test
    void getingDivisionOfTwoStringNumbers() {
        assertEquals("5", question1.getingDivisionOfTwoStringNumbers("25", "5"));
        assertEquals("56265659371326737", question1.getingDivisionOfTwoStringNumbers("281328296856633687", "5"));
        assertEquals("the value cannot be zero", question1.getingDivisionOfTwoStringNumbers("5", "0"));
    }

    @org.junit.jupiter.api.Test
    void getingSubstractionOfTwoStringNumbers() {
        assertEquals("45", question1.getingSubstractionOfTwoStringNumbers("50", "5"));
        assertEquals("400", question1.getingSubstractionOfTwoStringNumbers("600", "200"));
        assertEquals("281328296856077031", question1.getingSubstractionOfTwoStringNumbers("281328296856633687", "556656"));
    }

    @org.junit.jupiter.api.Test
    void addStrings() {
    }
}