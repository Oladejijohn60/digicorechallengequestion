package questionOneAndTwo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Question2Test {

    @Test
    void canCalculateScoreTest(){

       int [] exampleOneResult = Question2.calculateScore(new int[]{4, 1, 5, 6, 6}, new int[]{2, 5, 1, 0, 6});
        int [] expectedOutCome = {3,1};
       assertArrayEquals(expectedOutCome, exampleOneResult);

        int [] exampleTwoResult = Question2.calculateScore(new int[]{6, 3, 5, 6, 1}, new int[]{6, 1, 6, 4, 2});
        int [] expectedOutCome2 = {2,2};
        assertArrayEquals(expectedOutCome2, exampleTwoResult);



    }



}