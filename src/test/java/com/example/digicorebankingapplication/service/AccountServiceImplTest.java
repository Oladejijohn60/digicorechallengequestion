package com.example.digicorebankingapplication.service;

import com.example.digicorebankingapplication.data.model.Account;
import com.example.digicorebankingapplication.data.repository.AccountRepository;
import com.example.digicorebankingapplication.data.repository.AccountRepositoryImpl;
import com.example.digicorebankingapplication.dtos.request.CreateAccountRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class AccountServiceImplTest {

AccountService accountService = new AccountServiceImpl();


    AccountRepository accountRepository = new AccountRepositoryImpl();
    CreateAccountRequest createAccountRequest = new CreateAccountRequest();
    @Test
    void createAccount() throws Exception {

        createAccountRequest.setAccountName("olade");
        createAccountRequest.setInitialDeposit(900.0);
        createAccountRequest.setAccountPassword("12");
        accountService.createAccount(createAccountRequest);
        createAccountRequest.setAccountName("olade1");
        createAccountRequest.setInitialDeposit(600.0);
        createAccountRequest.setAccountPassword("123");
        accountService.createAccount(createAccountRequest);

        assertEquals(2,accountRepository.findAll().size());
    }

    @Test
    void createAccountMustHaveUniqueName() throws Exception {
        createAccountRequest.setAccountName("olade");
        createAccountRequest.setInitialDeposit(900.0);
        createAccountRequest.setAccountPassword("12");
        accountService.createAccount(createAccountRequest);

        assertNotNull(createAccountRequest);

        createAccountRequest.setAccountName("bolade");
        createAccountRequest.setInitialDeposit(600.0);
        createAccountRequest.setAccountPassword("123");


        assertNotNull(accountService.createAccount(createAccountRequest));
        assertEquals(2,accountRepository.findAll().size());


    }

    @Test
    void createAccountMustHaveInitialDepositAbove500() throws Exception {
        createAccountRequest.setAccountName("TESLIM");
        createAccountRequest.setInitialDeposit(876.0);
        createAccountRequest.setAccountPassword("12");
        accountService.createAccount(createAccountRequest);
        assertNotNull(createAccountRequest);
        createAccountRequest.setAccountName("TIFE");
        createAccountRequest.setInitialDeposit(700.0);
        createAccountRequest.setAccountPassword("123");
        assertNotNull(accountService.createAccount(createAccountRequest));
        assertEquals(2, accountRepository.findAll().size());


    }
    @Test
    void getAllAccounts() throws Exception {
        createAccountRequest.setAccountName("ADE");
        createAccountRequest.setInitialDeposit(900.0);
        createAccountRequest.setAccountPassword("12");
        accountService.createAccount(createAccountRequest);
        createAccountRequest.setAccountName("BINTU");
        createAccountRequest.setInitialDeposit(600.0);
        createAccountRequest.setAccountPassword("123");
        accountService.createAccount(createAccountRequest);

        assertNotNull(accountService.getAllAccounts());

    }

    @Test
    void getAccountInformation() {
    }

    @Test
    void withdraw() {
    }

    @Test
    void deposit() {
    }

    @Test
    void getAllAccountTransactions() {
    }
}