package questionOneAndTwo;

public class Question2 {
    public static int[] calculateScore(int[] player1, int[] player2) {
        int[] scores = new int[2];
        for (int index = 0; index < player1.length; index++) {
            if (player1[index] > player2[index]) {
                ++scores[0];
            } else {
                if (player1[index] < player2[index]) {
                    ++scores[1];
                }
            }

        }
        return scores;
    }
}
