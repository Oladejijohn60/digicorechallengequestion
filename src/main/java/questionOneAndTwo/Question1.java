package questionOneAndTwo;

public class Question1 {
    public static long getingValueOfString(String newNumber) {
        int counter = newNumber.length();
        int valueOfString = 0;
        long number = 0;
        for (int i = 0; i < newNumber.length(); i++) {
            counter -= 1;
            for (int j = counter; j <= counter; j++) {
                if (newNumber.charAt(j) == '0') valueOfString = 0;
                if (newNumber.charAt(j) == '1') valueOfString = 1;
                if (newNumber.charAt(j) == '2') valueOfString = 2;
                if (newNumber.charAt(j) == '3') valueOfString = 3;
                if (newNumber.charAt(j) == '4') valueOfString = 4;
                if (newNumber.charAt(j) == '5') valueOfString = 5;
                if (newNumber.charAt(j) == '6') valueOfString = 6;
                if (newNumber.charAt(j) == '7') valueOfString = 7;
                if (newNumber.charAt(j) == '8') valueOfString = 8;
                if (newNumber.charAt(j) == '9') valueOfString = 9;
                number += valueOfString * ((long) Math.pow(10, i));

            }

        }
        return number;
    }

    public  String getingSumOfTwoStringNumbers(String firstNumber, String secondNumber){
        return String.valueOf(getingValueOfString(firstNumber) + getingValueOfString(secondNumber));
    }
    public  String getingMultiplicationOfTwoStringNumbers(String firstNumber, String secondNumber){
        return String.valueOf(getingValueOfString(firstNumber) * getingValueOfString(secondNumber));
    }
    public  String getingDivisionOfTwoStringNumbers(String firstNumber, String secondNumber){
        if(getingValueOfString(secondNumber) != 0 ){
            return String.valueOf(getingValueOfString(firstNumber) / getingValueOfString(secondNumber));}
        else {
            return "the value cannot be zero"; }
    }
    public  String getingSubstractionOfTwoStringNumbers(String firstNumber, String secondNumber){
        return String.valueOf(getingValueOfString(firstNumber) - getingValueOfString(secondNumber));
    }


    public static String addStrings(String num1, String num2) {
        int total = 0;

        return String.valueOf(Long.parseLong(num1) + Long.parseLong(num2));
    }

}
