package com.example.digicorebankingapplication.data.repository;

import com.example.digicorebankingapplication.data.model.Account;

import java.util.List;

public interface AccountRepository {
    Account save(Account account);
    Account findAccountByAccountNumber(String name);
    void deleteAccountById(String id);
    void deleteAccount(Account account);
    List<Account> findAll();
    String findAccountNumberByName(String accountName);
}
