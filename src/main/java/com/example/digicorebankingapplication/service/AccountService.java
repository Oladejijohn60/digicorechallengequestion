package com.example.digicorebankingapplication.service;

import com.example.digicorebankingapplication.data.model.Account;
import com.example.digicorebankingapplication.data.model.AccountTransaction;
import com.example.digicorebankingapplication.dtos.request.CreateAccountRequest;
import com.example.digicorebankingapplication.dtos.request.DepositRequest;
import com.example.digicorebankingapplication.dtos.request.WithdrawRequest;
import com.example.digicorebankingapplication.dtos.response.AccountInformationResponse;
import com.example.digicorebankingapplication.dtos.response.CreateAccountResponse;
import com.example.digicorebankingapplication.dtos.response.TransactionResponse;
import com.example.digicorebankingapplication.exception.AccountDoesNotExistException;
import com.example.digicorebankingapplication.exception.ExcessWithdrawnAmountException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AccountService {
    CreateAccountResponse createAccount(CreateAccountRequest createAccountRequest) throws Exception;

    List<Account> getAllAccounts();
    AccountInformationResponse getAccountInformation(String accountNumber, String password );
    TransactionResponse withdraw(WithdrawRequest request) throws AccountDoesNotExistException, ExcessWithdrawnAmountException;
    TransactionResponse deposit(DepositRequest request);
    List<AccountTransaction> getAllAccountTransactions(String accountNumber);

}
