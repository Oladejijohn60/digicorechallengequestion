package com.example.digicorebankingapplication.dtos.request;

import lombok.Data;

@Data
public class CreateAccountRequest {
    String accountName;
    String accountPassword;
    Double initialDeposit;
}
