package com.example.digicorebankingapplication.dtos.request;

import lombok.Data;

@Data
public class DepositRequest {
    private String accountNumber;
    private Double amount;
}
