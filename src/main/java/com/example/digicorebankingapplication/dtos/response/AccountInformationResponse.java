package com.example.digicorebankingapplication.dtos.response;

import com.example.digicorebankingapplication.data.model.Account;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AccountInformationResponse {
    private int responseCode;
    private boolean success;
    private String message;
    private Account account;
}
