package com.example.digicorebankingapplication.dtos.response;

import lombok.Data;

@Data
public class CreateAccountResponse {
    int responseCode;
    boolean success;
    String message;
    String accountNumber;
}
