package com.example.digicorebankingapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigicoreBankingApplication {

    public static void main(String[] args) {
        SpringApplication.run(DigicoreBankingApplication.class, args);
    }

}
