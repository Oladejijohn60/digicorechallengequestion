package com.example.digicorebankingapplication.exception;

public class DuplicateAccountNameException extends  DigicoreBankingException {

    public DuplicateAccountNameException() {
    }

    public DuplicateAccountNameException(String message) {
        super(message);
    }
}
