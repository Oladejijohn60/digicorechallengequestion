package com.example.digicorebankingapplication.exception;

public class BadRequestException extends  DigicoreBankingException{
    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }
}
