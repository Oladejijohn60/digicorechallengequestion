package com.example.digicorebankingapplication.exception;

public class WrongPasswordException extends  DigicoreBankingException{
    public WrongPasswordException() {
    }

    public WrongPasswordException(String message) {
        super(message);
    }
}
