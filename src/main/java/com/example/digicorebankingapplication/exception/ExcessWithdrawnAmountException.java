package com.example.digicorebankingapplication.exception;

public class ExcessWithdrawnAmountException extends DigicoreBankingException{
    public ExcessWithdrawnAmountException() {
    }

    public ExcessWithdrawnAmountException(String message) {
        super(message);
    }
}
