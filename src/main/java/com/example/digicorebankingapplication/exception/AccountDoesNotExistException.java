package com.example.digicorebankingapplication.exception;

public class AccountDoesNotExistException extends DigicoreBankingException{
    public AccountDoesNotExistException() {
    }

    public AccountDoesNotExistException(String message) {
        super(message);
    }
}
