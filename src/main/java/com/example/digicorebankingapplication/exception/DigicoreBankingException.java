package com.example.digicorebankingapplication.exception;

public class DigicoreBankingException extends  RuntimeException{
    public DigicoreBankingException() {
    }

    public DigicoreBankingException(String message) {
        super(message);
    }
}
