package com.example.digicorebankingapplication.exception;

public class InitialDepositException extends DigicoreBankingException {
    public InitialDepositException() {
    }

    public InitialDepositException(String message) {
        super(message);
    }
}
