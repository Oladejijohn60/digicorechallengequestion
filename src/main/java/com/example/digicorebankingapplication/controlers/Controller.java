package com.example.digicorebankingapplication.controlers;

import com.example.digicorebankingapplication.dtos.request.CreateAccountRequest;
import com.example.digicorebankingapplication.dtos.request.DepositRequest;
import com.example.digicorebankingapplication.dtos.request.WithdrawRequest;
import com.example.digicorebankingapplication.dtos.response.TransactionResponse;
import com.example.digicorebankingapplication.exception.AccountDoesNotExistException;
import com.example.digicorebankingapplication.exception.DigicoreBankingException;
import com.example.digicorebankingapplication.exception.ExcessWithdrawnAmountException;
import com.example.digicorebankingapplication.exception.WrongPasswordException;
import com.example.digicorebankingapplication.service.AccountService;
import com.example.digicorebankingapplication.service.AccountServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class Controller {
    AccountService accountService = new AccountServiceImpl();

    @PostMapping("/api/vi/create_Account")
    public ResponseEntity<?> createAccount(@RequestBody CreateAccountRequest request) throws Exception {
        try {
            return new ResponseEntity<>(accountService.createAccount(request), HttpStatus.CREATED);
        }
        catch (DigicoreBankingException exception){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdraw(@RequestBody WithdrawRequest request){
        try {
            return new ResponseEntity<>(accountService.withdraw(request), HttpStatus.OK);
        } catch (AccountDoesNotExistException | ExcessWithdrawnAmountException | IllegalArgumentException e) {
            return new ResponseEntity<>(new TransactionResponse(false, e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/deposit")
    public ResponseEntity<?> deposit(@RequestBody DepositRequest request){
        try{
            return new ResponseEntity<>(accountService.deposit(request), HttpStatus.OK);
        }catch(IllegalArgumentException e){
            return new ResponseEntity<>(new TransactionResponse(false, e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/account_info/{accountNumber}")
    public ResponseEntity<?> getAccountInformation(@PathVariable String accountNumber, @RequestParam String password) {
        try {
            return new ResponseEntity<>(accountService.getAccountInformation(accountNumber, password), HttpStatus.OK);
        } catch (AccountDoesNotExistException | WrongPasswordException e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/account_statement/{accountNumber}")
    public ResponseEntity<?> getAccountStatement(@PathVariable String accountNumber){
        return new ResponseEntity<>(accountService.getAllAccountTransactions(accountNumber), HttpStatus.OK);
    }
}
